import java.util.ArrayList;

public class Phonebook {
    private ArrayList<Contact> contacts;

    public Phonebook() {
        contacts = new ArrayList<>();
    }


    public ArrayList<Contact> getContacts() {
        return contacts;
    }

    public void setContacts(ArrayList<Contact> contacts) {
        this.contacts = contacts;
    }

    public void addContact(Contact contact) {
        contacts.add(contact);
    }

    public boolean isEmpty() {
        return contacts.isEmpty();
    }

    public void displayContacts() {
        for (Contact contact : contacts) {
            System.out.println(contact.getName());
            System.out.println("---------------------");
            System.out.println(contact.getName() + " has the following registered number(s):");
            ArrayList<String> contactNumbers = contact.getContactNumbers();
            for (String number : contactNumbers) {
                System.out.println(number);
            }
            System.out.println(contact.getName() + " has the following registered address:");
            System.out.println(contact.getAddress());

        }
    }
}
