public class Main {
    public static void main(String[] args) {
        Phonebook phonebook = new Phonebook();

        Contact contact1 = new Contact("Jon Snow", "1234567890", "Castle Black");
        Contact contact2 = new Contact("Arya Stark", "0987645321", "Winterfell");

        phonebook.addContact(contact1);
        phonebook.addContact(contact2);

        if (phonebook.isEmpty()) {
            System.out.println("Phonebook is empty.");
        } else {
            System.out.println("My Phonebook");
            phonebook.displayContacts();
        }
    }
}
