package com.zuitt.example;

public class Animal {

    private String name;
    private String color;
    public int numberOfLegs = 4;

    //setters
    public void setName(String name){
        this.name = name;
    }
    public void setColor(String color){
        this.name = name;
    }

    public  String getName(){
        return name;
    }
    public  String getColor(){
        return color;
    }

    public Animal (){}

    public Animal(String name, String color){
        this.name = name;
        this.color = color;
    }
    public void call(){
        System.out.println("Hi my name is "+ this.name);
    }
    public Animal(String name, String color, int numberOfLegs){
        this.name = name;
        this.color = color;
        this.numberOfLegs = numberOfLegs;
    }

}
