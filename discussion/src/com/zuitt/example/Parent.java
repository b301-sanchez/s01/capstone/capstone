package com.zuitt.example;

//Dynamic or Run-time Polymorphism
public class Parent {
    public void speak(){
        System.out.println("I am the parent");
    }
}
