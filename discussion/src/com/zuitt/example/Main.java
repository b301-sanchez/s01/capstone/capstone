package com.zuitt.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args){

        Driver newDriver = new Driver();
        // Scanner input = new Scanner();
        //input.nextDouble();
        newDriver.setName("Aleck");
        // System.out.prinln(newDriver.name);
        System.out.println(newDriver.getName());

        //Driver newDriver2 = newDriver()

        //------

        Car myCar = new Car();
        System.out.print("The car is driven by " + myCar.getDriver());

        Car myCar2 = new Car("Model Y" , "Tesla", 2020);
        System.out.println(myCar2.getCarName());
        System.out.println(myCar2.getBrand());
        System.out.println(myCar2.getYearOfMake());
        System.out.println(myCar2.getDriver());

        //-------

        System.out.println("-------------");
        Animal newAnimal = new Animal();
        newAnimal.call();
        System.out.println(newAnimal.getName());
        System.out.println(newAnimal.getColor());

        Animal newAnimal2 = new Animal("Pica", "Yellow");
        newAnimal2.call();
        System.out.println(newAnimal2.getName());
        System.out.println(newAnimal2.getColor());

        //setters cam be used to set or updated values of a property
        newAnimal2.setName("Pikachu");
        System.out.println(newAnimal2.getName());
        System.out.println(newAnimal2.numberOfLegs);

        //--------
        Dog newDog = new Dog();
        newDog.printDogInfo();

        Dog newDog2 = new Dog("Browny", "brown","Shiba Inu");
        newDog2.printDogInfo();

        Person newPerson = new Person();
        newPerson.run();
        newPerson.sleep();

        //-------


        Child newChild = new Child();
        newChild.speak();

        //-------
        ArrayListClass newClassList = new ArrayListClass();
        newClassList.addStudent("Ronald");
        newClassList.addStudent("David Polo");
        newClassList.addStudent("Jayzher");
        System.out.println(newClassList.getStudents());

        /*ArrayList<String> newList = new ArrayList<>(List.of("John", "Jed", "Aj"));
        newClassList.setStudents(newList);
        System.out.println(newClassList.getStudents());*/


    }
}
//methods are basically function only inside an object or class