package com.zuitt.example;
// The "extends" keyword is used to establish inheritance relationship between classes, which is a pre requisite for achieving dynamic polymorphism

// Dynamic polymorphism(method overriding) allows methods of different class to exhibit different behaviours
public class Child extends Parent{
    //By using @Override, you explicitly indicate that the method is inteded to overide a class method.
    // This improves code readability and makes it easier for other developers to understand the code.

    @Override
    public void speak(){
        System.out.println("I am a mere child");
    }
}
